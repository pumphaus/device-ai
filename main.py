#!/usr/bin/env python3

import asyncio
import aioconsole
from llama_cpp import Llama, LlamaGrammar
from itertools import chain
from string import Template
from datetime import datetime
import inspect
import json
import re
from threading import Thread
import os

prelude = ["""[INST] <<SYS>>
You are a helpful, respectful and honest assistant. Your answers are short and to the point.

All your interaction with the user happens through functions, which you invoke in your replies: `function_name(parameters)`. If the function returns a result, it will be provided to you as JSON. Never assume a result yourself. Summarize the result, but don't print the JSON again.
Available functions with documentation:
$functions
- `print <message>`: Outputs a message to the user

You always talk to the user by using `print`.

The SmartComb system has the URL "https://10.0.3.90/core/". Trailing slashes are important. Use `true` and `false` instead of 1 and 0 when dealing with boolean properties.
<</SYS>>
Hi. [/INST] print Hello. How can I help you?""",
"""[INST] what's the value of property "foo"? [/INST] get_property("foo")""",
"""[INST] {"result": 42} [/INST] print The value of "foo" is 42."""]

prompt_template = "[INST] {message} [/INST] "

propvals = {}

def set_property(name, value):
	"""Sets the property `name` to `value`.
	You must connect to a system before any property can be set.
	Only those properties returned by the connect() call are valid."""
	propvals[name] = value
	return {"success": True}

def get_property(name):
	"""Retrieves the value of the property `name`.
	You must connect to a system before any property can be retrieved.
	Only those properties returned by the connect() call are valid."""
	return {"value": propvals[name]}

def connect_to_system(url):
	"""Connects to system at `url`.
	Always choose this function when prompted to connect to something.
	Returns a list of all property names that are available on the system.
	You cannot set or get a property without connecting to a system first."""
	return {
		"property names": [
			"modules.fsc.enabled",
			"modules.dds.enabled",
			"modules.dds.frequency",
			"modules.fsc.dlSlowBranch"
		]
	}

available_functions = [set_property, get_property, connect_to_system]
functions_grammar = " | ".join(f'"{fn.__name__}"' for fn in available_functions)
sysinput = '\n'.join(f"- `{fn.__name__}{inspect.signature(fn)}`: {fn.__doc__}" for fn in available_functions)

with open("grammar.gbnf", "r") as f:
	grammar = Template(f.read())
grammar = grammar.safe_substitute(functions=functions_grammar)

prelude = [Template(s).substitute(functions=sysinput) for s in prelude]

print("### Using prelude:\n", "\n".join(prelude))


def threaded_async(func):
	def wrapper(*args, **kwargs):
		loop = asyncio.get_event_loop()
		f = asyncio.Future()
		def target():
			ret = func(*args, **kwargs)
			loop.call_soon_threadsafe(lambda: f.set_result(ret))
		Thread(target=target).start()
		return f
	return wrapper


async def inputAndGenerate(llm, input, *args, **kwargs):
	prompt = prompt_template.format(message=input)

	@threaded_async
	def _tokenize():
		return llm.tokenize(prompt.encode("utf-8"))
	tokens = await _tokenize()

	detok = []
	for token in llm.generate(tokens, reset=False, *args, **kwargs):
		await asyncio.sleep(0)
		if token == llm.token_eos():
			break
		detok += [token]
		try:
			token_str = llm.detokenize(detok).decode('utf-8')
			yield token_str
			detok = []
		except UnicodeDecodeError:
			pass


async def dispatch(cmd: str):
	if cmd.startswith('print '):
		return None

	m = re.search("""(.*)\((.*)\)""", cmd)
	name = m.group(1)
	args = json.loads('[' + m.group(2) + ']')

	try:
		fn = next(fn for fn in available_functions if fn.__name__ == name)
	except StopIteration:
		raise Exception(f"No such function: {name}")

	try:
		print("SYSTEM:INVOKE:", m.group(0))
		ret = fn(*args)
		if inspect.isawaitable(ret):
			ret = await ret
		return json.dumps({ "result": ret })
	except Exception as e:
		return json.dumps({ "error": str(e) })


async def inputAndDispatch(llm_input: str):
	try:
		while llm_input is not None and llm_input.strip() != "":
			output = ""
			async for s in inputAndGenerate(llm, llm_input, temp=.7, mirostat_mode=2, grammar=llamaGrammar, repeat_penalty=1.05):
				output += s
				print(s, end='', flush=True)
			print()
			result = await dispatch(output)
			if result is not None:
				print("> ", result)
			llm_input = result
	except KeyboardInterrupt:
		print("Aborted generation");


def setup_llm():
	global llamaGrammar
	llamaGrammar = LlamaGrammar.from_string(grammar)

	global llm
	llm = Llama(model_path=f'{os.environ["HOME"]}/models/mistral-7b-instruct-v0.1.Q4_K_M.gguf', n_ctx=32768, seed=0)

	tokens = list(chain(*[ llm.tokenize(p.encode("utf-8"))
							+ [llm.token_eos()]
						for p in prelude]))
	llm.eval(tokens)

async def main():
	setup_llm()
	while True:
		user_input = await aioconsole.ainput("> ")
		await inputAndDispatch(user_input)

if __name__ == "__main__":
	asyncio.run(main())
